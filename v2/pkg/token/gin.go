package token

import (
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt/v4"
	ricardoErr "gitlab.com/ricardo-public/errors/pkg/errors"
	"net/http"
)

const (
	UserIDKey = "sub"
)

type AuthMiddleware interface {
	Authorize(c *gin.Context)
}

type jwtAuthMiddleware struct {
	key []byte
}

func NewJwtAuthMiddleware(key []byte) AuthMiddleware {
	return jwtAuthMiddleware{
		key: key,
	}
}

func (j jwtAuthMiddleware) Authorize(gtx *gin.Context) {
	token, err := ParseFromGinContext(gtx, j.key)
	if err != nil {
		_ = ricardoErr.GinErrorHandlerWithCode(gtx, err, http.StatusUnauthorized)
		return
	}

	claims, ok := Claims(token)
	if !ok {
		_ = ricardoErr.GinErrorHandlerWithCode(gtx, errors.New("cannot read claims"), http.StatusUnauthorized)
		return
	}

	sub, err := SubjectAsUint(*claims)
	if err != nil {
		_ = ricardoErr.GinErrorHandlerWithCode(gtx, err, http.StatusUnauthorized)
		return
	}

	gtx.Set(UserIDKey, sub)
	gtx.Next()

}

func (j jwtAuthMiddleware) HasAccess(gtx *gin.Context, giveAccess func(userID uint) bool) (bool, error) {
	token, err := ParseFromGinContext(gtx, j.key)
	if err != nil {
		return false, err
	}
	rClaims, ok := Claims(token)
	if !ok {
		return false, errors.New("cannot read claims")
	}

	if IsAdmin(*rClaims) {
		return true, nil
	}

	sub, err := SubjectAsUint(*rClaims)
	if err != nil {
		return false, err
	}
	return giveAccess(sub), nil
}

func ParseFromGinContext(gtx *gin.Context, key []byte) (*jwt.Token, error) {
	token := gtx.GetHeader(AuthorizationHeader)
	if token == "" {
		return nil, ricardoErr.New(ricardoErr.ErrUnauthorized, "you need to pass an access token")
	}

	if len(token) <= len(BearerType) {
		return nil, ricardoErr.New(ricardoErr.ErrUnauthorized, "access token format is invalid")
	}
	token = token[len(BearerType):]

	return Parse(token, key)
}
