package token

import (
	"errors"
	"github.com/golang-jwt/jwt/v4"
	"strconv"
	"time"
)

const (
	ErrParseToken   = "cannot parse token"
	ErrInvalidToken = "token is invalid"
)

type RicardoClaims struct {
	jwt.RegisteredClaims
	Role string `json:"role,omitempty"`
}

type Tokens struct {
	Access  jwt.Token `json:"access_token"`
	Refresh jwt.Token `json:"refresh_token"`
}

type SignedTokens struct {
	Access  string `json:"access_token"`
	Refresh string `json:"refresh_token"`
}

func NewRicardoClaims(sub, iss string, role Role, exp time.Time) RicardoClaims {
	return RicardoClaims{
		RegisteredClaims: jwt.RegisteredClaims{
			Subject:   sub,
			Issuer:    iss,
			ExpiresAt: jwt.NewNumericDate(exp),
		},
		Role: role.String(),
	}
}

func Parse(tokenStr string, key []byte) (*jwt.Token, error) {
	token, err := jwt.ParseWithClaims(tokenStr, &RicardoClaims{}, func(token *jwt.Token) (interface{}, error) {
		_, ok := token.Method.(*jwt.SigningMethodHMAC)
		if !ok {
			return nil, errors.New(ErrParseToken)
		}
		return key, nil
	})

	if err != nil {
		return nil, err
	}
	if !token.Valid {
		return nil, errors.New(ErrInvalidToken)
	}

	return token, nil
}

func GenerateHS256SignedToken(claims RicardoClaims, secret []byte) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString(secret)
}

func Claims(token *jwt.Token) (*RicardoClaims, bool) {
	rClaims, ok := token.Claims.(*RicardoClaims)
	if !ok {
		return nil, ok
	}

	return rClaims, ok
}

func SubjectAsUint(claims RicardoClaims) (uint, error) {
	uID, err := strconv.ParseUint(claims.Subject, 10, 64)
	if err != nil {
		return 0, err
	}

	return uint(uID), nil
}

func IsAdmin(claims RicardoClaims) bool {
	return Role(claims.Role) == AdminRole
}
