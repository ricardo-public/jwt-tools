package token

type Role string

const (
	UserRole  Role = "user"
	AdminRole Role = "admin"
)

func (r Role) String() string {
	return string(r)
}
