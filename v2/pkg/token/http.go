package token

import (
	"errors"
)

const (
	AuthorizationHeader = "Authorization"
	BearerType          = "Bearer "
)

func ExtractTokenFromHeader(token string) (string, error) {
	if len(token) <= len(BearerType) {
		return "", errors.New(ErrParseToken)
	}
	return token[len(BearerType):], nil
}
