package tokens

import (
	"errors"
	"github.com/gin-gonic/gin"
	ricardoErr "gitlab.com/ricardo-public/errors/pkg/errors"
	"net/http"
)

type AuthMiddleware interface {
	Authorize(c *gin.Context)
}

type jwtAuthMiddleware struct {
	key []byte
}

func NewJwtAuthMiddleware(key []byte) AuthMiddleware {
	return jwtAuthMiddleware{
		key: key,
	}
}

func (j jwtAuthMiddleware) Authorize(gtx *gin.Context) {
	token := gtx.GetHeader(AuthorizationHeader)
	if token == "" {
		ricardoErr.GinErrorHandlerWithCode(gtx, errors.New("you need to pass an access token"), http.StatusUnauthorized)
		return
	}

	if len(token) <= len(BearerType) {
		ricardoErr.GinErrorHandlerWithCode(gtx, errors.New("access token format is invalid"), http.StatusUnauthorized)
		return
	}
	token = token[len(BearerType):]

	_, err := Parse(token, j.key)
	if err != nil {
		_ = ricardoErr.GinErrorHandlerWithCode(gtx, err, http.StatusUnauthorized)
	} else {
		gtx.Next()
	}
}
