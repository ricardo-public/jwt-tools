package tokens

import (
	"errors"
	"github.com/golang-jwt/jwt"
)

const (
	AuthorizationHeader = "Authorization"
	BearerType          = "Bearer "

	AdminRole = "admin"

	ErrParseToken   = "cannot parse token"
	ErrInvalidToken = "token is invalid"
)

type RicardoClaims struct {
	jwt.StandardClaims
	Role string `json:"role,omitempty"`
}

func ExtractTokenFromHeader(token string) (string, error) {
	if len(token) <= len(BearerType) {
		return "", errors.New(ErrParseToken)
	}
	return token[len(BearerType):], nil
}

func Parse(tokenStr string, key []byte) (*jwt.Token, error) {
	token, err := jwt.ParseWithClaims(tokenStr, &RicardoClaims{}, func(token *jwt.Token) (interface{}, error) {
		_, ok := token.Method.(*jwt.SigningMethodHMAC)
		if !ok {
			return nil, errors.New(ErrParseToken)
		}
		return key, nil
	})

	if err != nil {
		return nil, err
	}
	if !token.Valid {
		return nil, errors.New(ErrInvalidToken)
	}

	return token, nil
}

func GenerateHS256SignedToken(claims jwt.Claims, secret []byte) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString(secret)
}
